﻿using System.ComponentModel.DataAnnotations;

namespace BlackJack.ViewModels.Game
{
    public class StartGameView
    {
        [Range(0,5, ErrorMessage = "Count of bots must be in range between 0 and 5.")]
        public int CountOfBots { get; set; }
    }
}
