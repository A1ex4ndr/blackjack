﻿using BlackJack.ViewModels.Enums;
using System.Collections.Generic;

namespace BlackJack.ViewModels.Game
{
    public class StateGameResponseView
    {
        public bool Finished { get; set; }
        public bool NeedInsurance { get; set; }
        public IEnumerable<PlayerStartGameResponseViewItem> Players { get; set; }
    }

    public class CardStartGameResponseViewItem
    {
        public CardNameViewType Name { get; set; }
        public CardSuitViewType Suit { get; set; }
    }

    public class PlayerStartGameResponseViewItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public RoleViewType Role { get; set; }
        public PlayerStatusViewType Status { get; set; }
        public int Points { get; set; }
        public IEnumerable<CardStartGameResponseViewItem> Cards { get; set; }
    }
}
