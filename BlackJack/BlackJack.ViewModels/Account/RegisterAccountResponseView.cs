﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJack.ViewModels.Account
{
    public class RegisterAccountResponseView
    {
        public string Token { get; set; }
        public string Username { get; set; }
    }
}
