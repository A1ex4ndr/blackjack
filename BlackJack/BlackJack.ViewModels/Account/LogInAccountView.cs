﻿namespace BlackJack.ViewModels.Account
{
    public class LogInAccountView
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
