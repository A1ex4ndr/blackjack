﻿namespace BlackJack.ViewModels.Enums
{
    public enum PlayerStatusViewType
    {
        None = 0,
        InGame = 1,
        Lose = 2,
        DeadHeat = 3,
        Win = 4,
        BlackJack = 5
    }
}
