﻿using BlackJack.BusinessLogic.Options;
using BlackJack.DataAccess;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace BlackJack.BusinessLogic.Configs
{
    public static class DbContextConfig
    {
        public static void ConfigureDbContext(this IServiceCollection services)
        {
            DbOptions dbOptions = null;
            using (var serviceProvider = services.BuildServiceProvider())
            {
                dbOptions = serviceProvider.GetService<DbOptions>();
            }
            if (dbOptions == null)
            {
                throw new ApplicationException("Db options is null.");
            }
            services.AddDbContext<BlackJackContext>(options => options.UseSqlServer(dbOptions.ConnectionString));
        }
    }
}
