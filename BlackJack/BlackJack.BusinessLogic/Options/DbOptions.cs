﻿using BlackJack.BusinessLogic.Enums;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJack.BusinessLogic.Options
{
    public class DbOptions
    {
        private readonly IConfiguration _configuration;

        public DbOptions(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string ConnectionString
        {
            get
            {
                var connectionName = _configuration.GetValue<string>("DbOption:ConnectionStringName");
                var connectionString = _configuration.GetValue<string>($"ConnectionStrings:{connectionName}");
                return connectionString;
            }
        }
        public OrmNameType OrmNameType
        {
            get
            {
                var ormName = _configuration.GetValue<string>("DbOption:OrmName");
                if (!Enum.TryParse(ormName, out OrmNameType ormNameType))
                {
                    throw new ApplicationException("Unknown type of ORM.");
                }
                return ormNameType;
            }
        }
    }
}
