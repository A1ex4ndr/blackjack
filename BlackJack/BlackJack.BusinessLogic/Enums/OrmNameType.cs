﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJack.BusinessLogic.Enums
{
    public enum OrmNameType
    {
        Dapper = 0,
        EntityFramework = 1
    }
}
