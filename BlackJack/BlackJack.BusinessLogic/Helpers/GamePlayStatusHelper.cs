﻿using BlackJack.BusinessLogic.Helpers.Interfaces;
using BlackJack.Entities.Entities;
using BlackJack.Entities.Enums;
using BlackJack.Entities.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace BlackJack.BusinessLogic.Helpers
{
    public class GamePlayStatusHelper : IGamePlayStatusHelper
    {
        public void UpdatePlayersStatusesOnInsurance(UserGamePlayer userGamePlayer, List<BotGamePlayer> botsGamePlayers, bool insuranceGame)
        {
            var dealerGamePlayer = botsGamePlayers.Where(g => g.Player.Type == BotType.Dealer).SingleOrDefault();
            var botPlayersGamePlayers = botsGamePlayers.Where(g => g.Player.Type == BotType.Player).ToList();
            if (dealerGamePlayer.Status == PlayerStatusType.BlackJack && (userGamePlayer.Status == PlayerStatusType.BlackJack || insuranceGame))
            {
                userGamePlayer.Status = PlayerStatusType.DeadHeat;
            }
            if (dealerGamePlayer.Status == PlayerStatusType.BlackJack && !insuranceGame)
            {
                userGamePlayer.Status = PlayerStatusType.Lose;
            }
            if (dealerGamePlayer.Status == PlayerStatusType.BlackJack)
            {
                foreach (var bot in botPlayersGamePlayers)
                {
                    bot.Status = PlayerStatusType.Lose;
                }
            }
        }

        public void UpdateGamePlayers(List<BotGamePlayer> botsGamePlayers, List<BotCard> botsCards, UserGamePlayer userGamePlayer, List<UserCard> userCards)
        {
            UpdateUserGamePlayer(userGamePlayer, userCards);
            UpdateBotsGamePlayers(botsGamePlayers, botsCards);
        }

        public void UpdateUserGamePlayer(UserGamePlayer gamePlayer, List<UserCard> playerCards)
        {
            gamePlayer.Points = CalculatePoints(playerCards.ToList<ICard>());
            gamePlayer.Status = StatusOnGameStep(gamePlayer.Points, playerCards.Count);
        }

        public void UpdateBotsGamePlayers(List<BotGamePlayer> gamePlayers, List<BotCard> cards)
        {
            foreach (var gamePlayer in gamePlayers)
            {
                var botCards = cards.Where(c => c.GamePlayerId == gamePlayer.Id)
                    .ToList<ICard>();
                gamePlayer.Points = CalculatePoints(botCards);
                gamePlayer.Status = StatusOnGameStep(gamePlayer.Points, botCards.Count);
            }
        }

        public PlayerStatusType StatusOnGameStep(int points, int cardsCount)
        {
            if (points > 21)
            {
                return PlayerStatusType.Lose;
            }
            if (points == 21 && cardsCount == 2)
            {
                return PlayerStatusType.BlackJack;
            }
            if (points == 21 && cardsCount != 2)
            {
                return PlayerStatusType.Win;
            }
            return PlayerStatusType.InGame;
        }

        public int CalculatePoints(List<ICard> cards)
        {
            int points = 0;
            int acesCount = 0;

            foreach (var card in cards)
            {
                var c = (int)card.CardName;
                if (c == 1)
                {
                    points += 11;
                    acesCount++;
                    continue;
                }
                if (c < 10)
                {
                    points += c;
                    continue;
                }
                points += 10;
            }
            if (points > 21)
            {
                points -= 10 * acesCount;
            }
            return points;
        }

        public void UpdateGamePlayersOnFinish(List<BotGamePlayer> botsGamePlayers, List<BotCard> botsCards, UserGamePlayer userGamePlayer, List<UserCard> userCards)
        {
            UpdateUserGamePlayer(userGamePlayer, userCards);
            UpdateBotsGamePlayers(botsGamePlayers, botsCards);

            var dealerGamePlayer = botsGamePlayers.Where(g => g.Player.Type == BotType.Dealer)
                .SingleOrDefault();
            var otherGamePlayers = botsGamePlayers.Where(g => g.Player.Type == BotType.Player)
                .ToList();

            foreach (var gamePlayer in otherGamePlayers)
            {
                gamePlayer.Status = PlayerStatusTypeOnFinish(gamePlayer, dealerGamePlayer);
            }
            userGamePlayer.Status = PlayerStatusTypeOnFinish(userGamePlayer, dealerGamePlayer);
        }

        public PlayerStatusType PlayerStatusTypeOnFinish(IGamePlayer gamePlayer, BotGamePlayer dealerGamePlayer)
        {
            if (gamePlayer.Status == PlayerStatusType.Lose)
            {
                return PlayerStatusType.Lose;
            }
            if ((gamePlayer.Status == PlayerStatusType.BlackJack && dealerGamePlayer.Status == PlayerStatusType.BlackJack)
                || (gamePlayer.Points == dealerGamePlayer.Points))
            {
                return PlayerStatusType.DeadHeat;
            }
            if ((gamePlayer.Status != PlayerStatusType.BlackJack && dealerGamePlayer.Status == PlayerStatusType.BlackJack)
                || (gamePlayer.Points < dealerGamePlayer.Points && dealerGamePlayer.Points <= 21))
            {
                return PlayerStatusType.Lose;
            }
            if (gamePlayer.Status == PlayerStatusType.BlackJack)
            {
                return PlayerStatusType.BlackJack;
            }
            //if (gamePlayer.Points > dealerGamePlayer.Points && gamePlayer.Points <= 21
            //    || (gamePlayer.Points <= 21 && dealerGamePlayer.Points > 21))
            //{
            //    gamePlayer.Status = PlayerStatusType.Win;
            //}
            return PlayerStatusType.Win;
        }
    }
}
