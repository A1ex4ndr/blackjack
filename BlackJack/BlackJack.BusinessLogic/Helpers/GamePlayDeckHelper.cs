﻿using BlackJack.BusinessLogic.Helpers.Interfaces;
using BlackJack.Entities.Entities;
using BlackJack.Entities.Enums;
using BlackJack.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BlackJack.BusinessLogic.Helpers
{
    public class GamePlayDeckHelper : IGamePlayDeckHelper
    {
        private readonly Random _random;

        public GamePlayDeckHelper()
        {
            _random = new Random();
        }

        public void CardDistribution(UserGamePlayer userGamePlayer, List<BotGamePlayer> botsGamePlayers,
            out List<UserCard> userCards, out List<BotCard> botsCards)
        {
            var allCards = new List<ICard>();
            userCards = new List<UserCard>();
            botsCards = new List<BotCard>();

            var tmpUserCard = GetNextUserCard(userGamePlayer, allCards);
            userCards.Add(tmpUserCard);
            allCards.Add(tmpUserCard);
            tmpUserCard = GetNextUserCard(userGamePlayer, allCards);
            userCards.Add(tmpUserCard);
            allCards.Add(tmpUserCard);

            foreach (var gamePlayer in botsGamePlayers)
            {
                var tmpBotCard = GetNextBotCard(gamePlayer, allCards);
                botsCards.Add(tmpBotCard);
                allCards.Add(tmpBotCard);
                tmpBotCard = GetNextBotCard(gamePlayer, allCards);
                botsCards.Add(tmpBotCard);
                allCards.Add(tmpBotCard);
            }
        }

        public BotCard GetNextBotCard(BotGamePlayer gamePlayer, List<ICard> cards)
        {
            var randomCard = RandomCard<BotCard>();
            while (cards.Contains(randomCard))
            {
                randomCard = RandomCard<BotCard>();
            }
            randomCard.GamePlayerId = gamePlayer.Id;
            randomCard.GamePlayer = gamePlayer;

            return randomCard;
        }

        public UserCard GetNextUserCard(UserGamePlayer gamePlayer, List<ICard> cards)
        {
            var randomCard = RandomCard<UserCard>();
            while (ContainsCard(cards, randomCard))
            {
                randomCard = RandomCard<UserCard>();
            }
            randomCard.GamePlayerId = gamePlayer.Id;
            randomCard.GamePlayer = gamePlayer;

            return randomCard;
        }

        public bool ContainsCard(List<ICard> cards, ICard card, byte countOfDecks = 1)
        {
            var count = cards.Where(c => c.CardName == card.CardName && c.CardSuit == card.CardSuit).Count();
            if (count > countOfDecks)
            {
                throw new ApplicationException("Count of same cards more than count of decks.");
            }
            if (count == countOfDecks)
            {
                return true;
            }
            return false;
        }

        public T RandomCard<T>()
            where T : ICard, new()
        {
            T card = new T();
            card.CardName = (CardNameType)_random.Next(1, 14);
            card.CardSuit = (CardSuitType)_random.Next(0, 4);
            return card;
        }

        public BotCard RemoveSecondDealerCard(List<BotGamePlayer> gamePlayers, List<BotCard> cards)
        {
            var secondDealerCard = GetSecondDealerCard(gamePlayers, cards);
            cards.Remove(secondDealerCard);
            return secondDealerCard;
        }

        public BotCard GetSecondDealerCard(List<BotGamePlayer> gamePlayers, List<BotCard> cards)
        {
            var dealerGamePlayer = gamePlayers.Where(gp => gp.Player.Type == BotType.Dealer).SingleOrDefault();
            var secondDealerCard = cards.Where(c => c.GamePlayerId == dealerGamePlayer.Id).LastOrDefault();
            return secondDealerCard;
        }
    }
}
