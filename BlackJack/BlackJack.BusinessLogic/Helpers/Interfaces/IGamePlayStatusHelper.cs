﻿using BlackJack.Entities.Entities;
using BlackJack.Entities.Enums;
using BlackJack.Entities.Interfaces;
using System.Collections.Generic;

namespace BlackJack.BusinessLogic.Helpers.Interfaces
{
    public interface IGamePlayStatusHelper
    {
        void UpdatePlayersStatusesOnInsurance(UserGamePlayer userGamePlayer, List<BotGamePlayer> botsGamePlayers, bool insuranceGame);
        void UpdateGamePlayers(List<BotGamePlayer> botsGamePlayers, List<BotCard> botsCards, UserGamePlayer userGamePlayer, List<UserCard> userCards);
        void UpdateUserGamePlayer(UserGamePlayer gamePlayer, List<UserCard> playerCards);
        void UpdateBotsGamePlayers(List<BotGamePlayer> gamePlayers, List<BotCard> cards);
        PlayerStatusType StatusOnGameStep(int points, int cardsCount);
        int CalculatePoints(List<ICard> cards);
        void UpdateGamePlayersOnFinish(List<BotGamePlayer> botsGamePlayers, List<BotCard> botsCards, UserGamePlayer userGamePlayer, List<UserCard> userCards);
        PlayerStatusType PlayerStatusTypeOnFinish(IGamePlayer gamePlayer, BotGamePlayer dealerGamePlayer);
    }
}
