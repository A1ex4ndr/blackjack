﻿using BlackJack.Entities.Entities;
using BlackJack.Entities.Interfaces;
using System.Collections.Generic;

namespace BlackJack.BusinessLogic.Helpers.Interfaces
{
    public interface IGamePlayDeckHelper
    {
        void CardDistribution(UserGamePlayer userGamePlayer, List<BotGamePlayer> botsGamePlayers, out List<UserCard> userCards, out List<BotCard> botsCards);
        BotCard GetNextBotCard(BotGamePlayer gamePlayer, List<ICard> cards);
        UserCard GetNextUserCard(UserGamePlayer gamePlayer, List<ICard> cards);
        bool ContainsCard(List<ICard> cards, ICard card, byte countOfDecks = 1);
        T RandomCard<T>() where T : ICard, new();
        BotCard RemoveSecondDealerCard(List<BotGamePlayer> gamePlayers, List<BotCard> cards);
        BotCard GetSecondDealerCard(List<BotGamePlayer> gamePlayers, List<BotCard> cards);
    }
}
