﻿using BlackJack.BusinessLogic.Services.Interfaces;
using BlackJack.DataAccess.Repositories.Interfaces;
using BlackJack.ViewModels.Enums;
using BlackJack.ViewModels.History;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJack.BusinessLogic.Services
{
    class HistoryService : IHistoryService
    {
        private readonly IBotCardRepository _botCardRepository;
        private readonly IBotGamePlayerRepository _botGamePlayerRepository;
        private readonly IBotPlayerRepository _botPlayerRepository;
        private readonly IGameRepository _gameRepository;
        private readonly IUserCardRepository _userCardRepository;
        private readonly IUserGamePlayerRepository _userGamePlayerRepository;
        private readonly IUserPlayerRepository _userPlayerRepository;

        public HistoryService(
            IBotCardRepository botCardRepository,
            IBotGamePlayerRepository botGamePlayerRepository,
            IBotPlayerRepository botPlayerRepository,
            IGameRepository gameRepository,
            IUserCardRepository userCardRepository,
            IUserGamePlayerRepository userGamePlayerRepository,
            IUserPlayerRepository userPlayerRepository
            )
        {
            _botCardRepository = botCardRepository;
            _botGamePlayerRepository = botGamePlayerRepository;
            _botPlayerRepository = botPlayerRepository;
            _gameRepository = gameRepository;
            _userCardRepository = userCardRepository;
            _userGamePlayerRepository = userGamePlayerRepository;
            _userPlayerRepository = userPlayerRepository;
        }

        public async Task<GetHistoryResponseView> AllHistory(string userId, GetHistoryView model)
        {
            var defaultPageSize = 10;
            var games = await _gameRepository.GetGamesOfUserOnPage(userId, model.Page, defaultPageSize);

            var gameItems = games.Select(g => new GameGetHistoryResponseViewItem()
            {
                Id = g.Id,
                CreationDate = g.CreationDate,
                Finished = g.Finished,
            }).ToList();

            var result = new GetHistoryResponseView();
            result.Games = gameItems;
            var gamesCount = await _gameRepository.GetGamesCountOfUser(userId);
            result.Pagination = new PaginationGetHistoryResponseViewItem() {
                Page = model.Page,
                TotalPagesCount = gamesCount / defaultPageSize + 1
            };

            return result;
        }

        public async Task<DetailsHistoryResponseView> GameDetails(string userId, long gameId)
        {
            var game = await _gameRepository.GetUserGameById(userId, gameId);
            if (game == null)
            {
                throw new ApplicationException("Game is null");
            }

            var userGamePlayer = await _userGamePlayerRepository.GetByGameId(game.Id);
            var botsGamePlayers = await _botGamePlayerRepository.GetAllByGameId(game.Id);

            var userCards = await _userCardRepository.GetAllByGameId(game.Id);
            var botsCards = await _botCardRepository.GetAllByGameId(game.Id);

            var result = new DetailsHistoryResponseView();
            result.GameId = game.Id;
            result.Finished = game.Finished;
            result.Insuranced = game.Insuranced;

            var gamePlayerItems = new List<PlayerDetailsHistoryResponseViewItem>();
            foreach (var botPlayer in botsGamePlayers)
            {
                var botCardsItems = botsCards.Where(c => c.GamePlayerId == botPlayer.Id)
                .Select(c => new CardDetailsHistoryResponseViewItem()
                {
                    Name = (CardNameViewType)c.CardName,
                    Suit = (CardSuitViewType)c.CardSuit,
                })
                .ToList();
                var item = new PlayerDetailsHistoryResponseViewItem()
                {
                    Id = botPlayer.Id,
                    Name = botPlayer.Player.Name,
                    Points = botPlayer.Points,
                    Status = (PlayerStatusViewType)botPlayer.Status,
                    Role = (RoleViewType)botPlayer.Player.Type,
                    Cards = botCardsItems
                };
                gamePlayerItems.Add(item);
            }

            var userCardsItems = userCards
                .Select(c => new CardDetailsHistoryResponseViewItem()
                {
                    Name = (CardNameViewType)c.CardName,
                    Suit = (CardSuitViewType)c.CardSuit
                })
                .ToList();
            gamePlayerItems.Add(new PlayerDetailsHistoryResponseViewItem()
            {
                Id = userGamePlayer.Id,
                Name = userGamePlayer.Player.Name,
                Role = RoleViewType.Player,
                Points = userGamePlayer.Points,
                Status = (PlayerStatusViewType)userGamePlayer.Status,
                Cards = userCardsItems
            });
            result.Players = gamePlayerItems;

            return result;
        }
    }
}
