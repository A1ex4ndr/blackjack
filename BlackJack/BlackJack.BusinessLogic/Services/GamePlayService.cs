﻿using BlackJack.BusinessLogic.Helpers.Interfaces;
using BlackJack.BusinessLogic.Services.Interfaces;
using BlackJack.DataAccess.Repositories.Interfaces;
using BlackJack.Entities.Entities;
using BlackJack.Entities.Enums;
using BlackJack.Entities.Interfaces;
using BlackJack.ViewModels.Enums;
using BlackJack.ViewModels.Game;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJack.BusinessLogic.Services
{
    public class GamePlayService : IGamePlayService
    {
        private readonly IBotCardRepository _botCardRepository;
        private readonly IBotGamePlayerRepository _botGamePlayerRepository;
        private readonly IBotPlayerRepository _botPlayerRepository;
        private readonly IGameRepository _gameRepository;
        private readonly IUserCardRepository _userCardRepository;
        private readonly IUserGamePlayerRepository _userGamePlayerRepository;
        private readonly IUserPlayerRepository _userPlayerRepository;
        private readonly IGamePlayDeckHelper _gamePlayDeckHelper;
        private readonly IGamePlayStatusHelper _gamePlayStatusHelper;
        private readonly UserManager<ApplicationUser> _userManager;

        private readonly Random _random;

        public GamePlayService(
            IBotCardRepository botCardRepository,
            IBotGamePlayerRepository botGamePlayerRepository,
            IBotPlayerRepository botPlayerRepository,
            IGameRepository gameRepository,
            IUserCardRepository userCardRepository,
            IUserGamePlayerRepository userGamePlayerRepository,
            IUserPlayerRepository userPlayerRepository,
            IGamePlayDeckHelper gamePlayDeckHelper,
            IGamePlayStatusHelper gamePlayStatusHelper,
            UserManager<ApplicationUser> userManager
        )
        {
            _botCardRepository = botCardRepository;
            _botGamePlayerRepository = botGamePlayerRepository;
            _botPlayerRepository = botPlayerRepository;
            _gameRepository = gameRepository;
            _userCardRepository = userCardRepository;
            _userGamePlayerRepository = userGamePlayerRepository;
            _userPlayerRepository = userPlayerRepository;
            _userManager = userManager;
            _gamePlayDeckHelper = gamePlayDeckHelper;
            _gamePlayStatusHelper = gamePlayStatusHelper;
            _random = new Random();
        }

        public async Task Start(string userId, StartGameView model)
        {
            var userPlayer = await _userPlayerRepository.GetByUserId(userId);
            if (userPlayer == null)
            {
                userPlayer = await InitUserPlayer(userId);
            }
            var notFinishedGame = await _gameRepository.GetNotFinishedGame(userPlayer.Id);
            if (notFinishedGame != null)
            {
                throw new ApplicationException("You have not fnished game.");
            }
            var game = new Game();
            await _gameRepository.Add(game);

            var botsPlayers = new List<BotPlayer>();

            var bots = await _botPlayerRepository.GetBots(model.CountOfBots);
            var dealer = await _botPlayerRepository.GetDealer();
            botsPlayers.Add(dealer);
            botsPlayers.AddRange(bots);

            InitGamePlayers(game, userPlayer, botsPlayers, out List<BotGamePlayer> botsGamePlayers, out UserGamePlayer userGamePlayer);
            await _userGamePlayerRepository.Add(userGamePlayer);
            await _botGamePlayerRepository.AddRange(botsGamePlayers);

            _gamePlayDeckHelper.CardDistribution(userGamePlayer, botsGamePlayers, out List<UserCard> userCards, out List<BotCard> botsCards);
            await _userCardRepository.AddRange(userCards);
            await _botCardRepository.AddRange(botsCards);
            var dealerSecondCard = _gamePlayDeckHelper.RemoveSecondDealerCard(botsGamePlayers, botsCards);

            _gamePlayStatusHelper.UpdateGamePlayers(botsGamePlayers, botsCards, userGamePlayer, userCards);

            if (userGamePlayer.Status != PlayerStatusType.InGame)
            {
                botsCards.Add(dealerSecondCard);
                var newBotsCards = BotsAndDealerFinishSteps(botsGamePlayers, botsCards, userCards);
                await _botCardRepository.AddRange(newBotsCards);
                botsCards.AddRange(newBotsCards);

                _gamePlayStatusHelper.UpdateGamePlayersOnFinish(botsGamePlayers, botsCards, userGamePlayer, userCards);

                game.Finished = true;
                await _gameRepository.Update(game);
            }
            await _botGamePlayerRepository.UpdateRange(botsGamePlayers);
            await _userGamePlayerRepository.Update(userGamePlayer);

            //var result = GetStateGameResponseView(botsGamePlayers, botsCards, userGamePlayer, userCards, game);
            //return result;
        }

        public async Task<StateGameResponseView> Hit(string userId)
        {
            var userPlayer = await _userPlayerRepository.GetByUserId(userId);
            if (userPlayer == null)
            {
                throw new ApplicationException("UserPlayer not found!");
            }
            var game = await _gameRepository.GetNotFinishedGame(userPlayer.Id);

            if (game == null)
            {
                throw new ApplicationException("Game not found.");
            }
            if (game.Finished)
            {
                throw new ApplicationException("Game finished.");
            }

            var botsGamePlayers = await _botGamePlayerRepository.GetAllByGameId(game.Id);
            var userGamePlayer = await _userGamePlayerRepository.GetByGameId(game.Id);

            var botsCards = await _botCardRepository.GetAllByGameId(game.Id);
            var userCards = await _userCardRepository.GetAllByGameId(game.Id);

            var insuranceExpected = InsuranceExpected(botsGamePlayers, botsCards);
            if (insuranceExpected)
            {
                throw new ApplicationException("Insurance expected.");
            }
            var allCards = botsCards.ToList<ICard>();
            allCards.AddRange(userCards);

            var card = _gamePlayDeckHelper.GetNextUserCard(userGamePlayer, allCards);
            userCards.Add(card);
            await _userCardRepository.Add(card);

            var dealerSecondCard = _gamePlayDeckHelper.RemoveSecondDealerCard(botsGamePlayers, botsCards);
            _gamePlayStatusHelper.UpdateUserGamePlayer(userGamePlayer, userCards);

            if (userGamePlayer.Status != PlayerStatusType.InGame)
            {
                botsCards.Add(dealerSecondCard);
                var newBotsCards = BotsAndDealerFinishSteps(botsGamePlayers, botsCards, userCards);
                botsCards.AddRange(newBotsCards);
                await _botCardRepository.AddRange(newBotsCards);

                _gamePlayStatusHelper.UpdateGamePlayersOnFinish(botsGamePlayers, botsCards, userGamePlayer, userCards);
                await _botGamePlayerRepository.UpdateRange(botsGamePlayers);

                game.Finished = true;
                await _gameRepository.Update(game);
            }

            await _userGamePlayerRepository.Update(userGamePlayer);

            var result = GetStateGameResponseView(botsGamePlayers, botsCards, userGamePlayer, userCards, game);
            return result;
        }

        public async Task<StateGameResponseView> Stand(string userId)
        {
            var userPlayer = await _userPlayerRepository.GetByUserId(userId);
            if (userPlayer == null)
            {
                throw new ApplicationException("UserPlayer not found!");
            }
            var game = await _gameRepository.GetNotFinishedGame(userPlayer.Id);
            if (game == null)
            {
                throw new ApplicationException("Game not found.");
            }
            if (game.Finished)
            {
                throw new ApplicationException("Game finished.");
            }

            var botsGamePlayers = await _botGamePlayerRepository.GetAllByGameId(game.Id);
            var userGamePlayer = await _userGamePlayerRepository.GetByGameId(game.Id);

            var botsCards = await _botCardRepository.GetAllByGameId(game.Id);
            var userCards = await _userCardRepository.GetAllByGameId(game.Id);

            var insuranceExpected = InsuranceExpected(botsGamePlayers, botsCards);
            if (insuranceExpected)
            {
                throw new ApplicationException("Insurance expected.");
            }

            var newCards = BotsAndDealerFinishSteps(botsGamePlayers, botsCards, userCards);
            await _botCardRepository.AddRange(newCards);
            botsCards.AddRange(newCards);

            _gamePlayStatusHelper.UpdateGamePlayersOnFinish(botsGamePlayers, botsCards, userGamePlayer, userCards);
            await _botGamePlayerRepository.UpdateRange(botsGamePlayers);
            await _userGamePlayerRepository.Update(userGamePlayer);

            game.Finished = true;
            await _gameRepository.Update(game);

            var result = GetStateGameResponseView(botsGamePlayers, botsCards, userGamePlayer, userCards, game);
            return result;
        }

        public async Task<StateGameResponseView> Insurance(string userId, InsuranceGameView model)
        {
            var userPlayer = await _userPlayerRepository.GetByUserId(userId);
            if (userPlayer == null)
            {
                throw new ApplicationException("UserPlayer not found!");
            }
            var game = await _gameRepository.GetNotFinishedGame(userPlayer.Id);
            if (game == null)
            {
                throw new ApplicationException("Game not found.");
            }
            if (game.Finished)
            {
                throw new ApplicationException("Game finished.");
            }

            var botsGamePlayers = await _botGamePlayerRepository.GetAllByGameId(game.Id);
            var botsCards = await _botCardRepository.GetAllByGameId(game.Id);

            var insuranceExpected = InsuranceExpected(botsGamePlayers, botsCards);
            if (!insuranceExpected)
            {
                throw new ApplicationException("Insurance not expected.");
            }

            var userGamePlayer = await _userGamePlayerRepository.GetByGameId(game.Id);
            var userCards = await _userCardRepository.GetAllByGameId(game.Id);

            var dealerGamePlayer = botsGamePlayers.Where(g => g.Player.Type == BotType.Dealer).SingleOrDefault();
            var botPlayersGamePlayers = botsGamePlayers.Where(g => g.Player.Type == BotType.Player).ToList();

            _gamePlayStatusHelper.UpdateGamePlayers(botsGamePlayers, botsCards, userGamePlayer, userCards);
            _gamePlayStatusHelper.UpdatePlayersStatusesOnInsurance(userGamePlayer, botsGamePlayers, model.Insurance);

            game.Insuranced = true;
            if (userGamePlayer.Status == PlayerStatusType.InGame)
            {
                _gamePlayDeckHelper.RemoveSecondDealerCard(botsGamePlayers, botsCards);
                _gamePlayStatusHelper.UpdateGamePlayers(botsGamePlayers, botsCards, userGamePlayer, userCards);
            }
            if (userGamePlayer.Status != PlayerStatusType.InGame)
            {
                var dealerSecondCard = _gamePlayDeckHelper.GetSecondDealerCard(botsGamePlayers, botsCards);
                game.Finished = true;
            }
            await _userGamePlayerRepository.Update(userGamePlayer);
            await _botGamePlayerRepository.UpdateRange(botsGamePlayers);
            await _gameRepository.Update(game);

            var result = GetStateGameResponseView(botsGamePlayers, botsCards, userGamePlayer, userCards, game);
            return result;
        }

        public async Task<StateGameResponseView> Load(string userId)
        {
            var userPlayer = await _userPlayerRepository.GetByUserId(userId);
            if (userPlayer == null)
            {
                throw new ApplicationException("UserPlayer not found!");
            }
            var notFinishedGame = await _gameRepository.GetNotFinishedGame(userPlayer.Id);
            if (notFinishedGame == null)
            {
                throw new ApplicationException("No game to load.");
            }
            var userGamePlayer = await _userGamePlayerRepository.GetByGameId(notFinishedGame.Id);
            var botsGamePlayers = await _botGamePlayerRepository.GetAllByGameId(notFinishedGame.Id);

            var userCards = await _userCardRepository.GetAllByGameId(notFinishedGame.Id);
            var botsCards = await _botCardRepository.GetAllByGameId(notFinishedGame.Id);
            _gamePlayDeckHelper.RemoveSecondDealerCard(botsGamePlayers, botsCards);

            var result = GetStateGameResponseView(botsGamePlayers, botsCards, userGamePlayer, userCards, notFinishedGame);
            return result;
        }

        public async Task<bool> HasNotFinishedGame(string userId)
        {
            var userPlayer = await _userPlayerRepository.GetByUserId(userId);
            if (userPlayer == null)
            {
                userPlayer = await InitUserPlayer(userId);
            }
            var hasNotFinishedGame = await _gameRepository.HasNotFinishedGame(userPlayer.Id);

            return hasNotFinishedGame;
        }

        private void InitGamePlayers(Game game, UserPlayer userPlayer, List<BotPlayer> botPlayers, out List<BotGamePlayer> botsGamePlayers, out UserGamePlayer userGamePlayer)
        {
            userGamePlayer = new UserGamePlayer()
            {
                GameId = game.Id,
                Game = game,
                PlayerId = userPlayer.Id,
                Player = userPlayer,
                Status = PlayerStatusType.InGame
            };

            botsGamePlayers = botPlayers.Select(x => new BotGamePlayer()
            {
                GameId = game.Id,
                Game = game,
                PlayerId = x.Id,
                Player = x,
                Points = 0,
                Status = PlayerStatusType.InGame
            }).ToList();
        }

        private List<BotCard> BotsAndDealerFinishSteps(List<BotGamePlayer> botGamePlayers, List<BotCard> botsCards, List<UserCard> userCards)
        {
            var allCards = botsCards.ToList<ICard>();
            allCards.AddRange(userCards);

            var newCards = new List<BotCard>();

            var bots = botGamePlayers.Where(gp => gp.Player.Type == BotType.Player)
                .ToList();
            var dealerGamePlayer = botGamePlayers.Where(gp => gp.Player.Type == BotType.Dealer)
                .SingleOrDefault();

            foreach (var botPlayer in bots)
            {
                var botCards = botsCards.Where(c => c.GamePlayerId == botPlayer.Id).ToList<ICard>();
                var newBotCards = BotFinishSteps(botPlayer, botCards, allCards);
                newCards.AddRange(newBotCards);
            }

            var dealerCards = botsCards.Where(c => c.GamePlayerId == dealerGamePlayer.Id)
                .ToList<ICard>();
            dealerGamePlayer.Points = _gamePlayStatusHelper.CalculatePoints(dealerCards);

            while (dealerGamePlayer.Points < 17)
            {
                var card = _gamePlayDeckHelper.GetNextBotCard(dealerGamePlayer, allCards);
                dealerCards.Add(card);
                newCards.Add(card);
                dealerGamePlayer.Points = _gamePlayStatusHelper.CalculatePoints(dealerCards);
            }
            return newCards;
        }

        private List<BotCard> BotFinishSteps(BotGamePlayer gamePlayer, List<ICard> botCards, List<ICard> allCards)
        {
            var newCards = new List<BotCard>();
            while (gamePlayer.Points < 20)
            {
                var p = 21 - gamePlayer.Points;

                if (_random.Next(0, 21) < p)
                {
                    var card = _gamePlayDeckHelper.GetNextBotCard(gamePlayer, allCards);
                    botCards.Add(card);
                    newCards.Add(card);
                    gamePlayer.Points = _gamePlayStatusHelper.CalculatePoints(botCards);
                    continue;
                }
                break;
            }
            return newCards;
        }

        private bool InsuranceExpected(List<BotGamePlayer> gamePlayers, List<BotCard> botsCards)
        {
            var dealerPlayer = gamePlayers.Where(gp => gp.Player.Type == BotType.Dealer).SingleOrDefault();
            var dealerCards = botsCards.Where(c => c.GamePlayerId == dealerPlayer.Id).ToList();
            var firstCard = dealerCards.FirstOrDefault();

            if ((firstCard.CardName == CardNameType.Ace || (int)(firstCard.CardName) > 9) && !dealerPlayer.Game.Insuranced)
            {
                return true;
            }
            return false;
        }

        private async Task<UserPlayer> InitUserPlayer(string userId)
        {
            var appUser = await _userManager.FindByIdAsync(userId);
            var userPlayer = new UserPlayer()
            {
                Name = appUser.UserName,
                UserId = userId
            };
            await _userPlayerRepository.Add(userPlayer);
            return userPlayer;
        }

        private StateGameResponseView GetStateGameResponseView(
            List<BotGamePlayer> botsGamePlayers,
            List<BotCard> botsCards,
            UserGamePlayer userGamePlayer,
            List<UserCard> userCards,
            Game game
            )
        {
            var result = new StateGameResponseView();
            result.Finished = game.Finished;
            result.NeedInsurance = InsuranceExpected(botsGamePlayers, botsCards);

            var gamePlayerItems = new List<PlayerStartGameResponseViewItem>();
            foreach (var botPlayer in botsGamePlayers)
            {
                var botCardsItems = botsCards?.Where(c => c.GamePlayerId == botPlayer.Id)
                .Select(c => new CardStartGameResponseViewItem()
                {
                    Name = (CardNameViewType)c.CardName,
                    Suit = (CardSuitViewType)c.CardSuit,
                })
                .ToList();
                var item = new PlayerStartGameResponseViewItem()
                {
                    Id = botPlayer.Id,
                    Name = botPlayer.Player.Name,
                    Points = botPlayer.Points,
                    Status = (PlayerStatusViewType)botPlayer.Status,
                    Role = (RoleViewType)botPlayer.Player.Type,
                    Cards = botCardsItems
                };
                gamePlayerItems.Add(item);
            }

            var userCardsItems = userCards?.Select(c => new CardStartGameResponseViewItem()
            {
                Name = (CardNameViewType)c.CardName,
                Suit = (CardSuitViewType)c.CardSuit
            });


            gamePlayerItems.Add(new PlayerStartGameResponseViewItem()
            {
                Id = userGamePlayer.Id,
                Name = userGamePlayer.Player.Name,
                Role = RoleViewType.Player,
                Points = userGamePlayer.Points,
                Status = (PlayerStatusViewType)userGamePlayer.Status,
                Cards = userCardsItems
            });

            result.Players = gamePlayerItems;
            return result;
        }
    }
}
