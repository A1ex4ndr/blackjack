﻿using BlackJack.BusinessLogic.Providers.Interfaces;
using BlackJack.BusinessLogic.Services.Interfaces;
using BlackJack.DataAccess.Repositories.Interfaces;
using BlackJack.Entities.Entities;
using BlackJack.ViewModels.Account;
using Microsoft.AspNetCore.Identity;
using System;
using System.Threading.Tasks;

namespace BlackJack.BusinessLogic.Services
{
    public class AccountService : IAccountService
    {
        private readonly IUserPlayerRepository _userPlayerRepository;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IJwtProvider _jwtProvider;

         public AccountService(UserManager<ApplicationUser> userManager, IJwtProvider jwtProvider, IUserPlayerRepository userPlayerRepository)
        {
            _userManager = userManager;
            _jwtProvider = jwtProvider;
            _userPlayerRepository = userPlayerRepository;
        }

        public async Task<LogInAccountResponseView> LogIn(LogInAccountView model)
        {
            var appUser = await _userManager.FindByNameAsync(model.Username);
            var passwordIsValid = await _userManager.CheckPasswordAsync(appUser, model.Password);

            if (!passwordIsValid)
            {
                throw new ApplicationException("Invalid username or password.");
            }

            var token = _jwtProvider.GenerateToken(appUser);

            var result = new LogInAccountResponseView()
            {
                Token = token,
                Username = appUser.UserName
            };
            return result;
        }

        public async Task<RegisterAccountResponseView> Register(RegisterAccountView model)
        {
            var user = await _userManager.FindByNameAsync(model.UserName);
            if (user != null)
            {
                throw new ApplicationException("Username is already taken.");
            }

            var appUser = new ApplicationUser()
            {
                UserName = model.UserName,
                Email = model.Email
            };

            var identityResult = await _userManager.CreateAsync(appUser, model.Password);

            var player = new UserPlayer()
            {
                Name = appUser.UserName,
                UserId = appUser.Id
            };

            if (!identityResult.Succeeded)
            {
                throw new ApplicationException("Registration exception.");
            }
            await _userPlayerRepository.Add(player);

            var token = _jwtProvider.GenerateToken(appUser);

            var result = new RegisterAccountResponseView()
            {
                Token = token,
                Username = appUser.UserName
            };
            return result;
        }
    }
}
