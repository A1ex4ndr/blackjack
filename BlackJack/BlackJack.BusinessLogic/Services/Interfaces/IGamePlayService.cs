﻿using System.Threading.Tasks;
using BlackJack.ViewModels.Game;

namespace BlackJack.BusinessLogic.Services.Interfaces
{
    public interface IGamePlayService
    {
        Task Start(string userId, StartGameView model);
        Task<StateGameResponseView> Hit(string userId);
        Task<StateGameResponseView> Stand(string userId);
        Task<StateGameResponseView> Insurance(string userId, InsuranceGameView model);
        Task<StateGameResponseView> Load(string userId);
        Task<bool> HasNotFinishedGame(string userId);
    }
}