﻿using BlackJack.ViewModels.History;
using System.Threading.Tasks;

namespace BlackJack.BusinessLogic.Services.Interfaces
{
    public interface IHistoryService
    {
        Task<GetHistoryResponseView> AllHistory(string userId, GetHistoryView model);
        Task<DetailsHistoryResponseView> GameDetails(string userId, long gameId);
    }
}
