﻿using BlackJack.ViewModels.Account;
using System.Threading.Tasks;

namespace BlackJack.BusinessLogic.Services.Interfaces
{
    public interface IAccountService
    {
        Task<LogInAccountResponseView> LogIn(LogInAccountView logInView);
        Task<RegisterAccountResponseView> Register(RegisterAccountView signInView);
    }
}
