﻿using BlackJack.Entities.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace BlackJack.Entities.Entities
{
    public class BaseEntity : IBaseEntity
    {
        [Key]
        public long Id { get; set; }
        public DateTime CreationDate { get; set; }

        public BaseEntity()
        {
            this.CreationDate = DateTime.UtcNow;
        }
    }
}
