﻿using BlackJack.Entities.Enums;
using BlackJack.Entities.Interfaces;
using Dapper.Contrib.Extensions;

namespace BlackJack.Entities.Entities
{
    public class BotCard : BaseEntity, ICard
    {
        public long GamePlayerId { get; set; }
        public CardNameType CardName { get; set; }
        public CardSuitType CardSuit { get; set; }

        [Write(false)]
        public virtual BotGamePlayer GamePlayer { get; set; }
    }
}
