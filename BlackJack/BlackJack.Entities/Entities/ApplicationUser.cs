﻿using BlackJack.Entities.Enums;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJack.Entities.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public UserRoleType Role { get; set; }
    }
}
