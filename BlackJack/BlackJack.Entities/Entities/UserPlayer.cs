﻿using BlackJack.Entities.Interfaces;
using Dapper.Contrib.Extensions;

namespace BlackJack.Entities.Entities
{
    public class UserPlayer : BaseEntity, IPlayer
    {
        public string Name { get; set; }
        public string UserId { get; set; }

        [Write(false)]
        public virtual ApplicationUser User { get; set; }
    }
}
