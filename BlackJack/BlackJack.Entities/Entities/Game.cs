﻿using System.ComponentModel.DataAnnotations.Schema;

namespace BlackJack.Entities.Entities
{
    public class Game : BaseEntity
    {
        public bool Finished { get; set; }
        public bool Insuranced { get; set; }
    }
}
