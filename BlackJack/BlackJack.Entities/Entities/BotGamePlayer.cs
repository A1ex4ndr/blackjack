﻿using BlackJack.Entities.Enums;
using BlackJack.Entities.Interfaces;
using Dapper.Contrib.Extensions;

namespace BlackJack.Entities.Entities
{
    public class BotGamePlayer : BaseEntity, IGamePlayer
    {
        public long GameId { get; set; }
        public long PlayerId { get; set; }
        public int Points { get; set; }
        public PlayerStatusType Status { get; set; }

        [Write(false)]
        public virtual Game Game { get; set; }
        [Write(false)]
        public virtual BotPlayer Player { get; set; }
    }
}
