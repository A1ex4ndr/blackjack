﻿using BlackJack.Entities.Enums;
using BlackJack.Entities.Interfaces;

namespace BlackJack.Entities.Entities
{
    public class BotPlayer : BaseEntity, IPlayer
    {
        public string Name { get; set; }
        public BotType Type { get; set; }
    }
}
