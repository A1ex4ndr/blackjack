﻿namespace BlackJack.Entities.Enums
{
    public enum BotType
    {
        Dealer = 0,
        Player = 1,
    }
}
