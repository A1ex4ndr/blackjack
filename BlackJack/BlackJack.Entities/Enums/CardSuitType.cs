﻿namespace BlackJack.Entities.Enums
{
    public enum CardSuitType
    {
        Diamonds = 0,
        Hearts = 1,
        Spades = 2,
        Clubs = 3
    }
}
