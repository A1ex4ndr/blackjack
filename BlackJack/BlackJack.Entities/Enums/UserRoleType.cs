﻿namespace BlackJack.Entities.Enums
{
    public enum UserRoleType
    {
        User = 0,
        Admin = 1
    }
}
