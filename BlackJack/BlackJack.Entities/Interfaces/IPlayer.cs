﻿namespace BlackJack.Entities.Interfaces
{
    public interface IPlayer : IBaseEntity
    {
        string Name { get; set; }
    }
}
