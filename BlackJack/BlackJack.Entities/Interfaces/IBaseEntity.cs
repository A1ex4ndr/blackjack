﻿using System;

namespace BlackJack.Entities.Interfaces
{
    public interface IBaseEntity
    {
        long Id { get; set; }
        DateTime CreationDate { get; set; }
    }
}
