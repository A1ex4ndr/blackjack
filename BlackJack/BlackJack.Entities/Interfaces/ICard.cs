﻿using BlackJack.Entities.Enums;

namespace BlackJack.Entities.Interfaces
{
    public interface ICard : IBaseEntity
    {
        long GamePlayerId { get; set; }
        CardNameType CardName { get; set; }
        CardSuitType CardSuit { get; set; }
    }
}
