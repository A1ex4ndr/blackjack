﻿using BlackJack.Entities.Enums;

namespace BlackJack.Entities.Interfaces
{
    public interface IGamePlayer : IBaseEntity
    {
        long GameId { get; set; }
        long PlayerId { get; set; }
        int Points { get; set; }
        PlayerStatusType Status { get; set; }
    }
}
