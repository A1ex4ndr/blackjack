﻿using BlackJack.DataAccess.Repositories.Interfaces;
using BlackJack.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories
{
    public class BotGamePlayerRepository : BaseRepository<BotGamePlayer>, IBotGamePlayerRepository
    {
        public BotGamePlayerRepository(BlackJackContext context) : base(context)
        {
        }

        public async Task<List<BotGamePlayer>> GetAllByGameId(long gameId)
        {
            var result = await _context.BotGamePlayers
                .Where(gp => gp.GameId == gameId)
                .ToListAsync();
            return result;
        }
    }
}
