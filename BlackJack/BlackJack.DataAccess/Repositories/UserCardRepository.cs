﻿using BlackJack.DataAccess.Repositories.Interfaces;
using BlackJack.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories
{
    public class UserCardRepository : BaseRepository<UserCard>, IUserCardRepository
    {
        public UserCardRepository(BlackJackContext context) : base(context)
        {
        }

        public async Task<List<UserCard>> GetAllByGameId(long gameId)
        {
            var result = await _context.UserCards
                .Join(_context.UserGamePlayers, c => c.GamePlayerId, gp => gp.Id, (c, gp) => new { Card = c, gp.GameId })
                .Where(p => p.GameId == gameId)
                .Select(p => p.Card)
                .ToListAsync();
            return result;
        }
    }
}
