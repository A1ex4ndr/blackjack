﻿using BlackJack.Entities.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories.Interfaces
{
    public interface IUserCardRepository : IBaseRepository<UserCard>
    {
        Task<List<UserCard>> GetAllByGameId(long gameId);
    }
}
