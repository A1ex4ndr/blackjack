﻿using BlackJack.Entities.Entities;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories.Interfaces
{
    public interface IUserGamePlayerRepository : IBaseRepository<UserGamePlayer>
    {
        Task<UserGamePlayer> GetByGameId(long gameId);
    }
}
