﻿using BlackJack.Entities.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories.Interfaces
{
    public interface IBotPlayerRepository : IBaseRepository<BotPlayer>
    {
        Task<List<BotPlayer>> GetBots(int count);
        Task<BotPlayer> GetDealer();
    }
}
