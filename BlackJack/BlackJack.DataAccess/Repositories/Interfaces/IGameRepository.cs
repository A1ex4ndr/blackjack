﻿using BlackJack.Entities.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories.Interfaces
{
    public interface IGameRepository : IBaseRepository<Game>
    {
        Task<bool> HasNotFinishedGame(long playerId);
        Task<Game> GetNotFinishedGame(long playerId);
        Task<List<Game>> GetAllGamesOfUser(string userId);
        Task<List<Game>> GetGamesOfUserOnPage(string userId, int pageNumber, int pageSize);
        Task<int> GetGamesCountOfUser(string userId);
        Task<Game> GetUserGameById(string userId, long gameId);
    }
}
