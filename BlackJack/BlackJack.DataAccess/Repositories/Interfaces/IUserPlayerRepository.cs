﻿using BlackJack.Entities.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories.Interfaces
{
    public interface IUserPlayerRepository : IBaseRepository<UserPlayer>
    {
        Task<UserPlayer> GetByUserId(string userId);
    }
}
