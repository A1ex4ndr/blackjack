﻿using BlackJack.Entities.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories.Interfaces
{
    public interface IBotGamePlayerRepository : IBaseRepository<BotGamePlayer>
    {
        Task<List<BotGamePlayer>> GetAllByGameId(long gameId);
    }
}
