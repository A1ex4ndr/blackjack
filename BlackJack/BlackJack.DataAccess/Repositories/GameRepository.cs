﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlackJack.DataAccess.Repositories.Interfaces;
using BlackJack.Entities.Entities;
using Microsoft.EntityFrameworkCore;

namespace BlackJack.DataAccess.Repositories
{
    public class GameRepository : BaseRepository<Game>, IGameRepository
    {
        public GameRepository(BlackJackContext context) : base(context)
        {
        }

        public async Task<List<Game>> GetAllGamesOfUser(string userId)
        {
            var result = await _context.Games
                .Join(_context.UserGamePlayers, g => g.Id, gp => gp.GameId, (g, gp) => new { Game = g, GamePlayerId = gp.PlayerId })
                .Join(_context.UserPlayers, p => p.GamePlayerId, u => u.Id, (p, u) => new { p.Game, u.UserId })
                .Where(p => p.UserId == userId)
                .Select(p => p.Game)
                .ToListAsync();
            return result;
        }

        public async Task<int> GetGamesCountOfUser(string userId)
        {
            var result = await _context.Games
               .Join(_context.UserGamePlayers, g => g.Id, gp => gp.GameId, (g, gp) => new { Game = g, GamePlayerId = gp.PlayerId })
               .Join(_context.UserPlayers, p => p.GamePlayerId, u => u.Id, (p, u) => new { p.Game, u.UserId })
               .Where(p => p.UserId == userId)
               .CountAsync();
            return result;
        }

        public async Task<List<Game>> GetGamesOfUserOnPage(string userId, int pageNumber, int pageSize)
        {
            var result = await _context.Games
                .Join(_context.UserGamePlayers, g => g.Id, gp => gp.GameId, (g, gp) => new { Game = g, GamePlayerId = gp.PlayerId })
                .Join(_context.UserPlayers, p => p.GamePlayerId, u => u.Id, (p, u) => new { p.Game, u.UserId })
                .Where(p => p.UserId == userId)
                .Select(p => p.Game)
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .ToListAsync();

            return result;
        }

        public async Task<Game> GetNotFinishedGame(long playerId)
        {
            var result = await _context.Games
                .Join(_context.UserGamePlayers, g => g.Id, gp => gp.GameId, (g, gp) => new { Game = g, GamePlayerId = gp.PlayerId })
                .Join(_context.UserPlayers, p => p.GamePlayerId, u => u.Id, (p, u) => new { p.Game, u.Id })
                .Where(p => p.Id == playerId && !p.Game.Finished)
                .Select(p => p.Game)
                .FirstOrDefaultAsync();
            return result;
        }

        public async Task<Game> GetUserGameById(string userId, long gameId)
        {
            var result = await _context.Games
                .Join(_context.UserGamePlayers, g => g.Id, gp => gp.GameId, (g, gp) => new { Game = g, GamePlayerId = gp.PlayerId })
                .Join(_context.UserPlayers, p => p.GamePlayerId, u => u.Id, (p, u) => new { p.Game, u.UserId })
                .Where(p => p.UserId == userId && p.Game.Id == gameId)
                .Select(p => p.Game)
                .FirstOrDefaultAsync();
            return result;
        }

        public async Task<bool> HasNotFinishedGame(long playerId)
        {
            var result = await _context.Games
               .Join(_context.UserGamePlayers, g => g.Id, gp => gp.GameId, (g, gp) => new { Game = g, GamePlayerId = gp.PlayerId })
               .Join(_context.UserPlayers, p => p.GamePlayerId, u => u.Id, (p, u) => new { p.Game, u.Id })
               .Where(p => p.Id == playerId && !p.Game.Finished)
               .CountAsync();
            return result != 0;
        }
    }
}
