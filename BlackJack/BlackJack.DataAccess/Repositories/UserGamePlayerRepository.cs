﻿using BlackJack.DataAccess.Repositories.Interfaces;
using BlackJack.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories
{
    public class UserGamePlayerRepository : BaseRepository<UserGamePlayer>, IUserGamePlayerRepository
    {
        public UserGamePlayerRepository(BlackJackContext context) : base(context)
        {
        }

        public async Task<UserGamePlayer> GetByGameId(long gameId)
        {
            var result = await _context.UserGamePlayers
                .Where(gp => gp.GameId == gameId)
                .FirstOrDefaultAsync();
            return result;
        }
    }
}
