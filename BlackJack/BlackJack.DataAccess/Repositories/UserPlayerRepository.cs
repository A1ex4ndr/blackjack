﻿using System.Linq;
using System.Threading.Tasks;
using BlackJack.DataAccess.Repositories.Interfaces;
using BlackJack.Entities.Entities;
using Microsoft.EntityFrameworkCore;

namespace BlackJack.DataAccess.Repositories
{
    public class UserPlayerRepository : BaseRepository<UserPlayer>, IUserPlayerRepository
    {
        public UserPlayerRepository(BlackJackContext context) : base(context)
        {
        }

        public async Task<UserPlayer> GetByUserId(string userId)
        {
            var result = await _context.UserPlayers
                .Where(u => u.UserId == userId)
                .FirstOrDefaultAsync();
            return result;
        }
    }
}
