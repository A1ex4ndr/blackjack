﻿using BlackJack.DataAccess.Repositories.Interfaces;
using BlackJack.Entities.Entities;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories.Dapper
{
    public class UserCardRepository : BaseRepository<UserCard>, IUserCardRepository
    {
        public UserCardRepository(IDbConnection connection) : base(connection)
        {
        }

        public async Task<List<UserCard>> GetAllByGameId(long gameId)
        {
            var sql = @"select A.*, B.* from UserCards as A
                            join UserGamePlayers as B on A.GamePlayerId = B.Id
	                        where B.GameId = @gameId";
            var userPlayersDictionary = new Dictionary<long, UserGamePlayer>();
            var result = await _connection.QueryAsync<UserCard, UserGamePlayer, UserCard>(sql,
                (userCard, userGamePlayer) =>
                {
                    if (!userPlayersDictionary.TryGetValue(userGamePlayer.Id, out UserGamePlayer outUserGamePlayer))
                    {
                        userPlayersDictionary.Add(userGamePlayer.Id, userGamePlayer);
                    }
                    if (outUserGamePlayer != null)
                    {
                        userGamePlayer = outUserGamePlayer;
                    }
                    userCard.GamePlayer = userGamePlayer;
                    return userCard;
                },
                param: new { gameId });
            return result.ToList();
        }
    }
}
