﻿using BlackJack.DataAccess.Repositories.Interfaces;
using BlackJack.Entities.Entities;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories.Dapper
{
    public class BotCardRepository : BaseRepository<BotCard>, IBotCardRepository
    {
        public BotCardRepository(IDbConnection connection) : base(connection)
        {
        }

        public async Task<List<BotCard>> GetAllByGameId(long gameId)
        {
            var sql = @"select A.*, B.* from BotCards as A
	                        join BotGamePlayers as B on A.GamePlayerId = B.Id
	                        where B.GameId = @gameId";

            var botPlayersDictionary = new Dictionary<long, BotGamePlayer>();
            var result = await _connection.QueryAsync<BotCard, BotGamePlayer, BotCard>(sql,
                (botCard, botGamePlayer) =>
                {
                    if (!botPlayersDictionary.TryGetValue(botGamePlayer.Id, out BotGamePlayer outBotGamePlayer))
                    {
                        botPlayersDictionary.Add(botGamePlayer.Id, botGamePlayer);
                    }
                    if (outBotGamePlayer != null)
                    {
                        botGamePlayer = outBotGamePlayer;
                    }
                    botCard.GamePlayer = botGamePlayer;
                    return botCard;
                },
                param: new { gameId });
            return result.ToList();
        }
    }
}
