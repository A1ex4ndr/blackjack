﻿using BlackJack.DataAccess.Repositories.Interfaces;
using BlackJack.Entities.Entities;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories.Dapper
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity>
        where TEntity : BaseEntity, new()
    {
        protected IDbConnection _connection;

        public BaseRepository(IDbConnection connection)
        {
            _connection = connection;
        }

        public async Task Add(TEntity entity)
        {
            entity.Id = await _connection.InsertAsync(entity);
        }

        public async Task<List<TEntity>> GetAll()
        {
            var result = await _connection.GetAllAsync<TEntity>();
            return result.ToList();
        }

        public async Task<TEntity> GetById(long id)
        {
            var result = await _connection.GetAsync<TEntity>(id);
            return result;
        }

        public async Task Remove(TEntity entity)
        {
            await _connection.DeleteAsync(entity);
        }

        public async Task Update(TEntity entity)
        {
            await _connection.UpdateAsync(entity);
        }

        public async Task AddRange(List<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                entity.Id = await _connection.InsertAsync(entity);
            }
        }

        public async Task RemoveRange(List<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                await _connection.DeleteAsync(entity);
            }
        }

        public async Task UpdateRange(List<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                await _connection.UpdateAsync(entity);
            }
        }
    }
}
