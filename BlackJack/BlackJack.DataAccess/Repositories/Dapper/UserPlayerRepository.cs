﻿using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using BlackJack.DataAccess.Repositories.Interfaces;
using BlackJack.Entities.Entities;
using Dapper;

namespace BlackJack.DataAccess.Repositories.Dapper
{
    public class UserPlayerRepository : BaseRepository<UserPlayer>, IUserPlayerRepository
    {
        public UserPlayerRepository(IDbConnection connection) : base(connection)
        {
        }

        public async Task<UserPlayer> GetByUserId(string userId)
        {
            var sql = @"select A.* from UserPlayers as A 
                            where A.UserId = @userId";
                var result = await _connection.QueryFirstOrDefaultAsync<UserPlayer>(sql, new { userId });
                return result;
        }
    }
}
