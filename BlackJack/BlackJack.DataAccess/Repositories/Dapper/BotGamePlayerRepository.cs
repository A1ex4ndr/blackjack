﻿using BlackJack.DataAccess.Repositories.Interfaces;
using BlackJack.Entities.Entities;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories.Dapper
{
    public class BotGamePlayerRepository : BaseRepository<BotGamePlayer>, IBotGamePlayerRepository
    {
        public BotGamePlayerRepository(IDbConnection connection) : base(connection)
        {
        }

        public async Task<List<BotGamePlayer>> GetAllByGameId(long gameId)
        {
            var sql = @"select A.*, B.*, C.* from BotGamePlayers as A
                            join Games as B on A.GameId = B.Id
                            join BotPlayers as C on A.PlayerId = C.Id
                            where A.GameId = @gameId";

            Game ggame = null;
            var playersDictionary = new Dictionary<long, BotPlayer>();
            var result = await _connection.QueryAsync<BotGamePlayer, Game, BotPlayer, BotGamePlayer>(sql,
                (botGamePlayer, game, player) =>
                {
                    if (ggame == null)
                    {
                        ggame = game;
                    }
                    if (!playersDictionary.TryGetValue(player.Id, out BotPlayer outBotPlayer))
                    {
                        playersDictionary.Add(player.Id, player);
                    }
                    if (outBotPlayer != null)
                    {
                        player = outBotPlayer;
                    }
                    botGamePlayer.Game = ggame;
                    botGamePlayer.Player = player;

                    return botGamePlayer;
                },
                param: new { gameId }
            );
            return result.ToList();
        }
    }
}
