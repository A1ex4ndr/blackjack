﻿using BlackJack.DataAccess.Repositories.Interfaces;
using BlackJack.Entities.Entities;
using BlackJack.Entities.Enums;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories.Dapper
{
    public class BotPlayerRepository : BaseRepository<BotPlayer>, IBotPlayerRepository
    {
        public BotPlayerRepository(IDbConnection connection) : base(connection)
        {
        }

        public async Task<List<BotPlayer>> GetBots(int count)
        {
            var sql = @"select top (@count) A.* from BotPlayers as A
	                        where A.Type = @type";
            var result = await _connection.QueryAsync<BotPlayer>(sql, new { count, type = BotType.Player });
            return result.ToList();
        }

        public async Task<BotPlayer> GetDealer()
        {
            var sql = @"select A.* from BotPlayers as A
	                        where A.Type = @type";
            var result = await _connection.QueryFirstOrDefaultAsync<BotPlayer>(sql, new { type = BotType.Dealer });
            return result;
        }
    }
}
