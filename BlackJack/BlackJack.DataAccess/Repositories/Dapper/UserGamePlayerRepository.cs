﻿using BlackJack.DataAccess.Repositories.Interfaces;
using BlackJack.Entities.Entities;
using Dapper;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace BlackJack.DataAccess.Repositories.Dapper
{
    public class UserGamePlayerRepository : BaseRepository<UserGamePlayer>, IUserGamePlayerRepository
    {
        public UserGamePlayerRepository(IDbConnection connection) : base(connection)
        {
        }

        public async Task<UserGamePlayer> GetByGameId(long gameId)
        {
            var sql = @"select A.*, B.*, C.* from UserGamePlayers as A
                            join Games as B on A.GameId = B.Id
                            join UserPlayers as C on A.PlayerId = C.Id
                            where A.GameId = @gameId";
            var result = await _connection.QueryAsync<UserGamePlayer, Game, UserPlayer, UserGamePlayer>(sql,
                (userGamePlayer, game, userPlayer) =>
                {
                    userGamePlayer.Game = game;
                    userGamePlayer.Player = userPlayer;

                    return userGamePlayer;
                },
                param: new { gameId });
            return result.FirstOrDefault();
        }
    }
}
