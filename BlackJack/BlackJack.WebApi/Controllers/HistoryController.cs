﻿using BlackJack.BusinessLogic.Services.Interfaces;
using BlackJack.ViewModels.History;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BlackJack.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [Authorize]
    public class HistoryController : BaseController
    {
        private readonly IHistoryService _historyService;

        public HistoryController(IHistoryService historyService)
        {
            _historyService = historyService;
        }

        [HttpPost]
        public async Task<IActionResult> FullHistory([FromBody]GetHistoryView model)
        {
            var result = await _historyService.AllHistory(UserId, model);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> GameDetails([FromBody]long gameId)
        {
            var result = await _historyService.GameDetails(UserId, gameId);
            return Ok(result);
        }
    }
}