﻿using BlackJack.BusinessLogic.Configs;
using BlackJack.BusinessLogic.Options;
using BlackJack.WebApi.Filters;
using BlackJack.WebApi.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace BlackJack.WebApi
{
    public class Startup
    {
        private readonly IHostingEnvironment _env;
        private readonly IServiceScopeFactory _serviceFactory;

        public Startup(IHostingEnvironment env, IServiceScopeFactory scopeFactory)
        {
            _env = env;
            _serviceFactory = scopeFactory;

            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsetting.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(config =>
            {
                var corsPolicy = new CorsPolicy();
                corsPolicy.Headers.Add("*");
                corsPolicy.Methods.Add("*");
                corsPolicy.Origins.Add("*");
                config.AddPolicy("MyCorsPolicy", corsPolicy);
            });
            services.Configure<JwtOption>(Configuration.GetSection("JwtOptions"));
            services.AddSingleton<DbOptions>();

            services.ConfigureDbContext();
            services.ConfigureIdentity();
            services.ConfigureDependencyInjections(Configuration);
            var clientAppRootPath = Configuration.GetValue<string>("ClientAppRootPath");

            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = clientAppRootPath;
            });
            services.AddMvc(options =>
            {
                options.Filters.Add<ModelStateFilter>();
            })
            .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "BlackJack API"
                    
                });
            });
        }
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHsts();
            app.UseCors("MyCorsPolicy");
            app.UseMiddleware<ExceptionHandlerMiddleware>();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseSpaStaticFiles();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "BlackJack API");
            });

            app.UseMvc();

            var clientAppRootPath = Configuration.GetValue<string>("ClientAppRootPath");
            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = clientAppRootPath;
            });
        }
    }
}
