﻿using Microsoft.AspNetCore.Http;
using System;
using System.Net;
using System.Threading.Tasks;

namespace BlackJack.WebApi.Middlewares
{
    public class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionHandlerMiddleware(RequestDelegate next)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (ApplicationException exception)
            {
                await HandleException(context, HttpStatusCode.BadRequest, exception);
            }
            catch (Exception exception)
            {
                await HandleException(context, HttpStatusCode.InternalServerError);
            }
        }

        private async Task HandleException(HttpContext context, HttpStatusCode statusCode, Exception exception = null)
        {
            context.Response.Clear();

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)statusCode;
            var error = exception?.Message ?? "Internal server error.";
            await context.Response.WriteAsync(error);
        }   
    }
}
