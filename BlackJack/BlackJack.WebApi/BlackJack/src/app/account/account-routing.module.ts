import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogInComponent } from './log-in/log-in.component';
import { RegisterComponent } from './register/register.component';
import { NoAuthGuard } from '../shared/guards/no-auth.guard';
import { AccountComponent } from './account.component';

const routes: Routes = [
  {
    path: '', component: AccountComponent, canActivate: [NoAuthGuard],
    children: [
      { path: '', redirectTo: 'auth' },
      { path: 'auth', loadChildren: './log-in/log-in.module#LogInModule', canActivate: [NoAuthGuard] },
      { path: 'register', loadChildren: './register/register.module#RegisterModule', canActivate: [NoAuthGuard] }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
