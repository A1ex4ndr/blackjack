import { Component, OnInit, Injector, inject } from '@angular/core';
import { AccountService } from 'src/app/shared/services/account.service';
import { Router } from '@angular/router';
import { RegisterAccountView } from 'src/app/shared/models/account/register-account-view';
import { RegisterAccountResponseView } from 'src/app/shared/models/account/register-account-response-view';
import { LocalStorageDataService } from 'src/app/shared/services/local-storage-data.service';
import { localStorageDataKeysConfig } from 'src/app/shared/configs/local-storage-data-keys.config';
import { BaseComponent } from 'src/app/shared/components/base-component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AccountValidators } from 'src/app/shared/form-validators/account-validators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent extends BaseComponent {

  model: RegisterAccountView = new RegisterAccountView();
  registerForm: FormGroup;
  submitted: boolean;

  constructor(
    injector: Injector,
    private storageService: LocalStorageDataService,
    private accountService: AccountService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    super(injector);
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(6)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirmPassword: ['', [Validators.required]]
    }, {
        validator: AccountValidators.mustMatch('password','confirmPassword')
    });
  }
  onRegisterSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    this.executeObservable(this.accountService.register(this.model)).subscribe((result: RegisterAccountResponseView) => {
      if (result) {
        this.storageService.setValue(localStorageDataKeysConfig.user, result);
        this.router.navigate(['/home']);
      }
    });
  }

  get formControls() {
    return this.registerForm.controls;
  }
}
