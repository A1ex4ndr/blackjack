import { Component, OnInit, Injector } from '@angular/core';
import { AccountService } from 'src/app/shared/services/account.service';
import { Router } from '@angular/router';
import { LocalStorageDataService } from 'src/app/shared/services/local-storage-data.service';
import { localStorageDataKeysConfig } from 'src/app/shared/configs/local-storage-data-keys.config';
import { LogInAccountResponseView } from 'src/app/shared/models/account/log-in-account-response-view';
import { LogInAccountView } from 'src/app/shared/models/account/log-in-account-view';
import { BaseComponent } from 'src/app/shared/components/base-component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent extends BaseComponent {

  submitted: boolean;
  logInForm: FormGroup;
  model: LogInAccountView = new LogInAccountView();

  constructor(
    injector: Injector,
    private accountService: AccountService,
    private storageService: LocalStorageDataService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.logInForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(6)]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
  }

  onLogInSubmit(): void {
    this.submitted = true;

    if (this.logInForm.invalid) {
      return;
    }

    this.executeObservable(this.accountService.logIn(this.model)).subscribe((result: LogInAccountResponseView) => {
      if (result) {
        this.storageService.setValue(localStorageDataKeysConfig.user, result);
        this.router.navigate(['/home']);
      }
    });
  }
  get formControls() {
    return this.logInForm.controls;
  }
}
