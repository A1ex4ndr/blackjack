import { Injectable } from '@angular/core';
import { CanActivate, UrlTree } from '@angular/router';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { GameService } from '../services/game.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class PlayGuard implements CanActivate {

  constructor(
    private router: Router,
    private gameService: GameService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>  {
    //const result: boolean = await this.gameService.hasNotFinished().toPromise();
    //if (!result) {
    //  this.router.navigateByUrl('/game/start');
    //}
    //return result;
    return this.gameService.hasNotFinished().pipe(
      map((data: boolean) =>
      {
        if (!data) {
          this.router.navigateByUrl('/game/start');
        }
        return data;
      })
    );
  }
}
