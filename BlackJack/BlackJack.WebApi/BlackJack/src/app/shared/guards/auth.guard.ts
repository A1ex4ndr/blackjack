import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LocalStorageDataService } from '../services/local-storage-data.service';
import { localStorageDataKeysConfig } from '../configs/local-storage-data-keys.config';
import { LogInAccountResponseView } from '../models/account/log-in-account-response-view';
import { AccountService } from '../services/account.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private accountService: AccountService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const authenticated = this.accountService.authenticated();
    if (authenticated) {
      return true;
    }
    this.router.navigate(['account/auth']);
    return false;
  }
}
