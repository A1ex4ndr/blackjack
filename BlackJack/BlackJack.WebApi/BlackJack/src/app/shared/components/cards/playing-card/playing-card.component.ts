import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-playing-card',
  templateUrl: './playing-card.component.html',
  styleUrls: ['./playing-card.component.css']
})
export class PlayingCardComponent implements OnInit {

  constructor() { }
  @Input() cardSymbol: string;
  @Input() suitImgSrc: string;

  ngOnInit() {

  }
}
