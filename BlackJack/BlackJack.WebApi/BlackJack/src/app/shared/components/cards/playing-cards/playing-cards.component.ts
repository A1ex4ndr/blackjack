import { Component, OnInit, Input } from '@angular/core';
import { Card } from 'src/app/shared/interfaces/card.interface';
import { CardNameViewType } from 'src/app/shared/models/enums/card-name-view-type';
import { CardSuitViewType } from 'src/app/shared/models/enums/card-suit-view-type';

@Component({
  selector: 'app-playing-cards',
  templateUrl: './playing-cards.component.html',
  styleUrls: ['./playing-cards.component.css']
})
export class PlayingCardsComponent implements OnInit {

  @Input() cards: Card[];

  constructor() { }

  ngOnInit() {
  }

  getCardSymbol(cardName: CardNameViewType) {
    if (cardName > 1 && cardName < 11) {
      return cardName.toString();
    }
    return CardNameViewType[cardName].charAt(0);
  }
  getImgSrc(cardSuit: CardSuitViewType) {
    return `assets/img/suits/${CardSuitViewType[cardSuit]}.jpg`;
  }
}
