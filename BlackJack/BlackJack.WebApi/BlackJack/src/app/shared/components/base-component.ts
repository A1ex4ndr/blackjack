import { ToastrService } from 'ngx-toastr';
import { OnInit, Injector, Component } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { map, catchError, finalize } from 'rxjs/operators';

export class BaseComponent implements OnInit {

  private toastr: ToastrService;
  private loaderService: NgxUiLoaderService;
  public loading: boolean;

  ngOnInit(): void {
  }
  constructor(injector: Injector) {
    this.toastr = injector.get(ToastrService);
    this.loaderService = injector.get(NgxUiLoaderService);
  }
  public showSuccess(message: string) {
    this.toastr.success(message, 'Success!');
    this.toastr.show();
  }

  public showError(message: string) {
    this.toastr.error(message, 'Oops!');
  }

  public showWarning(message: string) {
    this.toastr.warning(message, 'Alert!');
  }

  public showInfo(message: string) {
    this.toastr.info(message);
  }

  public startLoader() {
    this.loaderService.start();
  }
  public stopLoader() {
    this.loaderService.stop();
  }

  public executeObservable<T>(func: Observable<T>, loaderEnabled: boolean = true): Observable<{} | T> {
    if (loaderEnabled) {
      this.startLoader();
    }
    this.loading = true;
    return func.pipe(
      map(
        (success: T) => {
          return success;
        }
      ),
      catchError(error => {
        this.stopLoader();
        if (error instanceof HttpErrorResponse && typeof error.error === "string") {
          this.showError(error.error);
        }
        if (error instanceof HttpErrorResponse && typeof error.error !== "string") {
          this.showError(error.message);
        }
        return null;
      }),
      finalize(() => {
        this.loading = false;
        this.stopLoader();
      })
    );
  }
}
