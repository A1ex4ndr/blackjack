import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {

  private countOfPages: number;
  values: number[];

  @Input() current: number;
  @Input() size: number;
  @Input() set totalCountOfPages(n: number) {
    if (!n) {
      return;
    }
    this.countOfPages = n;
    if (this.size > n) {
      this.size = n;
    this.currentPage = this.current;
    }
  }
  get totalCountOfPages() {
    return this.countOfPages;
  }

  @Output() pageChanged: EventEmitter<number> = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {

    if (this.current < 1 || this.current > this.totalCountOfPages) {
      this.current = 1;
    }
    if (this.size < 1) {
      this.size = 3;
    }
  }

  get hidden(): boolean {
    if (!this.totalCountOfPages || this.totalCountOfPages < 2) {
      return true;
    }
    return false;
  }

  set currentPage(n: number) {
    this.values = [];
    this.current = n;
    let start = Math.round(this.current - this.size / 2);
    if (start + this.size > this.totalCountOfPages) {
      start = this.totalCountOfPages - this.size + 1;
    }
    if (start < 1) {
      start = 1;
    }
    for (let i = 0; i < this.size; i++ , start++) {
      this.values.push(start);
    }
    this.pageChanged.emit(this.current);
  }

  get firstValue() {
    return this.values[0];
  }
  get lastValue() {
    return this.values[this.values.length - 1];
  }
}
