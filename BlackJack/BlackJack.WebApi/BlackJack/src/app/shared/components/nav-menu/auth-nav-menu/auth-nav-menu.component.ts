import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/shared/services/account.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth-nav-menu',
  templateUrl: './auth-nav-menu.component.html',
  styleUrls: ['./auth-nav-menu.component.css']
})
export class AuthNavMenuComponent implements OnInit {

  constructor(private accountService: AccountService, private router: Router) { }

  username: string;

  ngOnInit() {
    this.username = this.accountService.getUsername();
  }

  logOut() {
    this.accountService.logOut();
    location.reload(true);
  }

}
