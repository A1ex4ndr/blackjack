import { CardNameViewType } from '../enums/card-name-view-type';
import { CardSuitViewType } from '../enums/card-suit-view-type';
import { PlayerStatusViewType } from '../enums/player-status-view-type';
import { RoleViewType } from '../enums/role-view-type';
import { Card } from '../../interfaces/card.interface';

export class DetailsHistoryResponseView {
  public gameId: number;
  public finished: boolean;
  public insuranced: boolean;
  public players: PlayerDetailsHistoryResponseViewItem[];
  constructor() {
    this.players = [];
  }
}
export class CardDetailsHistoryResponseViewItem implements Card {
  public name: CardNameViewType;
  public suit: CardSuitViewType;
}
export class PlayerDetailsHistoryResponseViewItem {
  public id: number;
  public name: string;
  public status: PlayerStatusViewType;
  public points: number;
  public role: RoleViewType;
  public cards: CardDetailsHistoryResponseViewItem[];

  constructor() {
    this.cards = [];
  }
}
