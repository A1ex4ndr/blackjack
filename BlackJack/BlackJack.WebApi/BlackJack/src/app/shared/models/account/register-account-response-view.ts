export class RegisterAccountResponseView {
  public token: string;
  public username: string;
}
