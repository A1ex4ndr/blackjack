import { CardNameViewType } from '../enums/card-name-view-type';
import { CardSuitViewType } from '../enums/card-suit-view-type';
import { RoleViewType } from '../enums/role-view-type';
import { PlayerStatusViewType } from '../enums/player-status-view-type';
import { Card } from '../../interfaces/card.interface';

export class StateGameResponseView {
  public finished: boolean;
  public needInsurance: boolean;
  public players: PlayerStartGameResponseViewItem[];

  constructor() {
    this.players = [];
  }
}
export class CardStartGameResponseViewItem implements Card {
  public name: CardNameViewType;
  public suit: CardSuitViewType;
}
export class PlayerStartGameResponseViewItem {
  public id: number;
  public name: string;
  public role: RoleViewType;
  public status: PlayerStatusViewType;
  public points: number;
  public cards: CardStartGameResponseViewItem[];

  constructor() {
    this.cards = [];
  }
}
