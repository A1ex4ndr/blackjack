export enum PlayerStatusViewType {
  InGame = 0,
  Lose = 1,
  DeadHeat = 2,
  Win = 3,
  BlackJack = 4
}
