import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetHistoryResponseView } from '../models/history/get-history-response-view';
import { DetailsHistoryResponseView } from '../models/history/details-history-response-view';
import { environment } from 'src/environments/environment';
import { GetHistoryView } from '../models/history/get-history-view';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {
  constructor(private http : HttpClient) { }

  getAll(model: GetHistoryView) : Observable<GetHistoryResponseView> {
    return this.http.post<GetHistoryResponseView>(`${environment.apiUrl}/history/fullhistory`, model);
  }
  getGameDetails(gameId: number) : Observable<DetailsHistoryResponseView> {
    return this.http.post<DetailsHistoryResponseView>(`${environment.apiUrl}/history/gamedetails`, gameId);
  }
}
