import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { LogInAccountResponseView } from '../models/account/log-in-account-response-view';
import { LogInAccountView } from '../models/account/log-in-account-view';
import { RegisterAccountResponseView } from '../models/account/register-account-response-view';
import { RegisterAccountView } from '../models/account/register-account-view';
import { LocalStorageDataService } from './local-storage-data.service';
import { localStorageDataKeysConfig } from '../configs/local-storage-data-keys.config';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  constructor(private http: HttpClient, private localStorageService: LocalStorageDataService) {
  }

  logIn(model: LogInAccountView): Observable<LogInAccountResponseView> {
    return this.http.post<LogInAccountResponseView>(`${environment.apiUrl}/account/login`, model);
  }
  register(model: RegisterAccountView): Observable<RegisterAccountResponseView> {
    return this.http.post<RegisterAccountResponseView>(`${environment.apiUrl}/account/register`, model);
  }
  authenticated(): boolean {
    const user = this.localStorageService.getValue<LogInAccountResponseView>(localStorageDataKeysConfig.user);
    if (user && user.token) {
      return true;
    }
    return false;
  }
  getUsername() : string {
    const user = this.localStorageService.getValue<LogInAccountResponseView>(localStorageDataKeysConfig.user);
    return user.username;
  }
  logOut() {
    this.localStorageService.removeValue(localStorageDataKeysConfig.user);
  }
}
