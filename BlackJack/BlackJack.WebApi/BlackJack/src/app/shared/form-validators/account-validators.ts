import { FormGroup } from '@angular/forms';

export class AccountValidators {

  static mustMatch(firstControlName: string, secondControlName: string) {
    return function (formGroup: FormGroup) {
      const firstControl = formGroup.controls[firstControlName];
      const secondControl = formGroup.controls[secondControlName];

      if (firstControl.value !== secondControl.value) {
        secondControl.setErrors({ mustMatch: true });
      };
    }
  }
}
