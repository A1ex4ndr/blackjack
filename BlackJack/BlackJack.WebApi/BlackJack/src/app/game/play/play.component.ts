import { Component, Injector } from '@angular/core';
import { GameService } from 'src/app/shared/services/game.service';
import { InsuranceGameView } from 'src/app/shared/models/game/insurance-game-view';
import { StateGameResponseView } from 'src/app/shared/models/game/state-game-response-view';
import { BaseComponent } from 'src/app/shared/components/base-component';
import { PlayerStatusViewType } from 'src/app/shared/models/enums/player-status-view-type';
import { StartGameView } from 'src/app/shared/models/game/start-game-view';
import { RoleViewType } from 'src/app/shared/models/enums/role-view-type';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.css']
})
export class PlayComponent extends BaseComponent {

  gameDetails: StateGameResponseView;
  needInsure: boolean;
    constructor(
    injector: Injector,
    private gameService: GameService,
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.loadGame();
  }

  loadGame() {
    this.executeObservable(this.gameService.load()).subscribe((result: StateGameResponseView) => {
      if (result) {
        this.gameDetails = result;
        this.needInsure = result.needInsurance;
      }
    });
  }
  onPlayAgainClick() {
    let startModel: StartGameView = new StartGameView();
    startModel.countOfBots = this.gameDetails.players.length - 2;
    this.executeObservable(this.gameService.startGame(startModel)).subscribe(
      result => {
        if (result) {
          this.loadGame();
        }
      }
    );
  }
  onHitClick() {
    this.executeObservable(this.gameService.hit(), false).subscribe((result: StateGameResponseView) => {
      if (result) {
        this.processData(result);
      }
    });
  }
  onStandClick() {
    this.executeObservable(this.gameService.stand(), false).subscribe((result: StateGameResponseView) => {
      if (result) {
        this.processData(result);
      }
    });
  }
  onInsuranseClick(needInsurance: boolean) {
    this.needInsure = false;
    var body: InsuranceGameView = new InsuranceGameView();
    body.insurance = needInsurance;
    this.executeObservable(this.gameService.insurance(body), false).subscribe((result: StateGameResponseView) => {
      if (result) {
        this.processData(result);
      }
    });
  }

  getStatus(value: PlayerStatusViewType) {
    return PlayerStatusViewType[value];
  }
  getDealer() {
    return this.gameDetails.players.find(p => p.role == RoleViewType.Dealer);
  }
  getPlayers() {
    return this.gameDetails.players.filter(p => p.role != RoleViewType.Dealer);
  }

  private processData(data: StateGameResponseView) {
    this.gameDetails.finished = data.finished;
    data.players.forEach(element => {
      const gamePlayer = this.gameDetails.players.find(p => p.id === element.id && p.role === element.role);
      gamePlayer.points = element.points;
      gamePlayer.status = element.status;
      gamePlayer.cards = element.cards;
    });
  }

  dealerHasBlackJackOrLose(): boolean {
    const dealer = this.getDealer();
    if (dealer.status === PlayerStatusViewType.BlackJack) {
      return true;
    }
    if (dealer.status === PlayerStatusViewType.Lose) {
      return true;
    }
    return false;
  }
}
