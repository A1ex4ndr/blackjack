import { NgModule } from '@angular/core';
import { StartComponent } from './start.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [StartComponent],
  imports: [
    RouterModule.forChild([{ path: '', component: StartComponent }]),
    SharedModule,
  ]
})
export class StartModule { }
