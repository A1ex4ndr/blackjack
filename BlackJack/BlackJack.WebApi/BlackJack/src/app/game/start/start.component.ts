import { Component, OnInit, Injector } from '@angular/core';
import { GameService } from 'src/app/shared/services/game.service';
import { Router } from '@angular/router';
import { StartGameView } from 'src/app/shared/models/game/start-game-view';
import { BaseComponent } from 'src/app/shared/components/base-component';
import { AccountService } from 'src/app/shared/services/account.service';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.css']
})
export class StartComponent extends BaseComponent {

  model: StartGameView = new StartGameView();
  username: string;

  constructor(
    injector: Injector,
    private gameService: GameService,
    private router: Router,
    private accountService: AccountService
  ) {
    super(injector);
  }

  ngOnInit() {
    this.username = this.accountService.getUsername();
    this.model.countOfBots = 0;
  }

  onStartGameClick() {
    this.executeObservable(this.gameService.startGame(this.model)).subscribe((result: boolean) => {
      if (result) {
        this.router.navigate(['/game/play']);
      }
    });
  }
}
