import { NgModule } from '@angular/core';
import { GameRoutingModule } from './game-routing.module';
import { FormsModule } from '@angular/forms';
import { GameComponent } from './game.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [GameComponent],
  imports: [
    GameRoutingModule,
    FormsModule,
    SharedModule
  ],
  providers: [
  ]
})
export class GameModule { 
}
