import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../shared/guards/auth.guard';
import { HistoryComponent } from './history.component';

const routes: Routes = [
  {
    path: '', component: HistoryComponent, children:
      [
        { path: '', loadChildren: './full-history/full-history.module#FullHistoryModule', canActivate: [AuthGuard] },
        { path: ':gameId', loadChildren: './history-game-details/history-game-details.module#HistoryGameDetailsModule', canActivate: [AuthGuard] }
      ],
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HistoryRoutingModule { }
