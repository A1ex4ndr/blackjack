import { Component, Injector } from '@angular/core';
import { HistoryService } from 'src/app/shared/services/history.service';
import { Router } from '@angular/router';
import { GetHistoryResponseView } from 'src/app/shared/models/history/get-history-response-view';
import { BaseComponent } from 'src/app/shared/components/base-component';
import { ToastrService } from 'ngx-toastr';
import { GetHistoryView } from 'src/app/shared/models/history/get-history-view';

@Component({
  selector: 'app-history',
  templateUrl: './full-history.component.html',
  styleUrls: ['./full-history.component.css']
})
export class FullHistoryComponent extends BaseComponent {

  history: GetHistoryResponseView;
  model: GetHistoryView;

  constructor(
    injector: Injector,
    private historyService: HistoryService,
    private router: Router
  ) {
    super(injector);
  }
  ngOnInit() {
    this.model = new GetHistoryView();
    this.model.page = 1;
    this.onPageChanged(1);
  }
  ngAfterContentInit(): void {
  }
  onGetDetailsClick(gameId: number) {
    this.router.navigate([`history/${gameId}`], { skipLocationChange: true });
  }
  onPageChanged(page: number) {
    this.model.page = page;

    this.executeObservable(this.historyService.getAll(this.model)).subscribe((result: GetHistoryResponseView) => {
      if (result) {
        this.history = result;
      }
    });
  }
}
