import { Component, Injector } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HistoryService } from 'src/app/shared/services/history.service';
import { DetailsHistoryResponseView } from 'src/app/shared/models/history/details-history-response-view';
import { BaseComponent } from 'src/app/shared/components/base-component';
import { PlayerStatusViewType } from 'src/app/shared/models/enums/player-status-view-type';
import { RoleViewType } from 'src/app/shared/models/enums/role-view-type';

@Component({
  selector: 'app-history-game-details',
  templateUrl: './history-game-details.component.html',
  styleUrls: ['./history-game-details.component.css']
})
export class HistoryGameDetailsComponent extends BaseComponent {

  gameDetails: DetailsHistoryResponseView;

  constructor(
    injector: Injector,
    private activatedRoute: ActivatedRoute,
    private historyService: HistoryService,
    private router: Router
  ) {
    super(injector);
  }

  async ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      const gameId: number = params.gameId;
      this.executeObservable(this.historyService.getGameDetails(gameId)).subscribe((result: DetailsHistoryResponseView) => {
        if (result) {
          this.gameDetails = result;
        }
      });
    });
  }

  dealerHasBlackJackOrLose(): boolean {
    const dealer = this.getDealer();
    if (dealer.status === PlayerStatusViewType.BlackJack) {
      return true;
    }
    if (dealer.status === PlayerStatusViewType.Lose) {
      return true;
    }
    return false;
  }
  getDealer() {
    return this.gameDetails.players.find(p => p.role == RoleViewType.Dealer);
  }
  getPlayers() {
    return this.gameDetails.players.filter(p => p.role != RoleViewType.Dealer);
  }
  getStatus(status: PlayerStatusViewType) {
    return PlayerStatusViewType[status];
  }

  onBackToHistoryClick() {
    this.router.navigate(['/history']);
  }
}
