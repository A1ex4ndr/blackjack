import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HistoryGameDetailsComponent } from './history-game-details.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [HistoryGameDetailsComponent],
  imports: [
    SharedModule,
    RouterModule.forChild([{ path: '', component: HistoryGameDetailsComponent }]),
  ]
})
export class HistoryGameDetailsModule { }
