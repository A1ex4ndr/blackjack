import { Component, Injector } from '@angular/core';
import { BaseComponent } from '../shared/components/base-component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent extends BaseComponent {

  constructor(
    injector: Injector,
    private router: Router
  ) {
    super(injector);
  }

  ngOnInit() {
  }

  onPlayClick() {
    this.router.navigate(['/game']);
  }
  onAppStoreClick() {
    this.showInfo('Link temporary unavialable');
  }
  onGooglePlayClick() {
    this.showInfo('Link temporary unavialable');
  }

}
